/*
	Mini Activity

	Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keywoed assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor shoule be able to crate Dog objects.
	Log the 2 new Dog objects in the console or alert.


*/

/*

1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function

*/

let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name : "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
};

/*function introduce(student){

	//Note: You can destructure objects inside functions.

	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log("I study the following courses" + classes);
}*/


//Template literal
console.log(`Hi! I'm ${student1.name}. I am ${student1.age} years old`);
console.log(`I study the following courses ${student1.classes}`);
console.log(`Hi! I'm ${student2.name}. I am ${student2.age} years old`);
console.log(`I study the following courses ${student2.classes}`);

//object/array destructuring
let {name1,age1,classes1} = student1;
console.log(`Hi! I'm ${name1}. I am ${age1} years old`);
console.log(`I study the following courses ${classes1}`);

let {name2,age2,classes2} = student2;
console.log(`Hi! I'm ${name2}. I am ${age2} years old`);
console.log(`I study the following courses ${classes2}`);


//arrow function
const introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old`);
	console.log(`I study the following courses ${student.classes}`);
}
introduce(student1);
introduce(student2);


/*
function getCube(num){

	console.log(Math.pow(num,3));

}

let cube = getCube(3);

console.log(cube)
 */


const getCube = (num) => Math.pow(num,3);

let cube = getCube(3);
console.log(cube);


/*
let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})

*/

let numArr = [15,16,32,21,21,2]

numArr.forEach((num) =>{
	console.log(num);
});

/*
let numsSquared = numArr.map(function(num){
	return num ** 2;
})
console.log(numSquared);
*/

let numsSquared = numArr.map((num)=> num ** 2);

console.log(numsSquared);


/*
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keywoed assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.

*/

class Dog {
	constructor(name,breed,dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = (7 * dogAge);
	}
}

/*
3. Create 2 new objects using our class constructor
	This constructor shoule be able to crate Dog objects.
	Log the 2 new Dog objects in the console or alert.
*/

let dog1 = new Dog('Yoshi', 'Golden Retriever', 1);
let dog2 = new Dog('Shore', 'Husky', 2);
console.log(dog1);
console.log(dog2);


